module spacegame.go

go 1.22.2

replace spacegame/vector => ./util

require (
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20240307211618-a69d953ea142
	spacegame/vector v0.0.0-00010101000000-000000000000
)
