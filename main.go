package main

import (
	"fmt"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
	"log"
	"runtime"
	"spacegame.go/game/world"
	"spacegame/util/image"
	util "spacegame/util/math"
)

const windowWidth = 800
const windowHeight = 600

func init() {
	// This is needed to arrange that main() runs on main thread.
	// See documentation for functions that are only allowed to be called from the main thread.
	runtime.LockOSThread()
}

func main() {

	galaxy, err := world.GenerateGalaxy(world.GalaxyGenerationConfig{
		StarSystemCount: 5000,
		MapSize:         util.MakeVector[float32](200.0, 200.0, 200.0),
	})

	if err != nil {
		log.Fatalln("Couldn't generate galaxy:", err)
	}

	fmt.Println(galaxy)

	if err := glfw.Init(); err != nil {
		log.Fatalln("Couldn't initialize glfw:", err)
	}
	defer glfw.Terminate()

	fmt.Println(image.LoadImage("./test.jpg", 0).SumPixelValues(0))

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(windowWidth, windowHeight, "space-game", nil, nil)

	if err != nil {
		log.Fatalln("Couldn't create window:", err)
	}

	window.MakeContextCurrent()

	// Initialize Glow
	if err := gl.Init(); err != nil {
		log.Fatalln("Couldn't init opengl:", err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	fmt.Println("OpenGL version", version)

	return
}
