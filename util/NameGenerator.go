package util

import (
	"math/rand"
)

type Names struct {
	Decorators []string `json:"decorators"`
	Fragments  []string `json:"fragments"`
	Endings    []string `json:"endings"`
}

func (names Names) GetRandomName() string {

	name := ""
	if rand.Float32() > 0.8 {
		name += names.Decorators[rand.Intn(len(names.Decorators))] + " "
	}

	for range rand.Intn(3) + 1 {
		for range rand.Intn(3) + 1 {
			fragment := names.Fragments[rand.Intn(len(names.Fragments))]
			name += fragment
		}
		name += " "
	}

	if rand.Float32() > 0.8 {
		name += names.Endings[rand.Intn(len(names.Endings))]
	}

	return name
}
