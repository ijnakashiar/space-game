package math

// ArithmeticDefined Approximation elements types that have the following underlying types
type ArithmeticDefined interface {
	~int | ~int32 | ~int64 | ~float32 | ~float64 | ~uint | ~uint32 | ~uint64
}

type FloatingPointType interface {
	~float32 | ~float64
}

type IMatrix[T ArithmeticDefined] interface {
	Col(n int) Vector[T]
	Row(n int) Vector[T]
	Get(x, y int) *T
}
