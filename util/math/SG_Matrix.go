package math

import (
	"fmt"
	"log"
	"strings"
)

type Matrix[T ArithmeticDefined] struct {
	data    []T
	rowLen  int // More generically this can be an array that represents the stride in each dimension.
	numRows int
}

func MakeMatrix[T ArithmeticDefined](x, y int, elements ...T) (matrix Matrix[T]) {
	if x*y != len(elements) {
		panic("matrix size does not match element count")
	}
	matrix.rowLen = x
	matrix.numRows = y
	matrix.data = elements
	return matrix
}

func (mat Matrix[T]) Col(n int) Vector[T] {
	if n >= mat.rowLen {
		log.Panicf("bad access to matrix column: %#v\n", n)
	}

	v := make([]T, mat.numRows)

	for i := range mat.numRows {
		v[i] = mat.data[n+mat.rowLen*i]
	}

	return Vector[T]{v[:]}
}

func (mat Matrix[T]) Row(n int) Vector[T] {
	if n >= mat.numRows {
		log.Panicf("bad access to matrix row: %#v\n", n)
	}
	return Vector[T]{mat.data[mat.rowLen*n : mat.rowLen*(n+1)]}
}

func (mat Matrix[T]) Get(x, y int) *T {
	if x >= mat.rowLen || y >= mat.numRows {
		log.Panicf("bad access to matrix element at %#v,%#v", x, y)
	}
	return &mat.data[mat.rowLen*y+x]
}

// Mul Matrix maybe we should just import a separate library for this.
// This isn't cache optimal
func (mat Matrix[T]) Mul(other Matrix[T]) (res Matrix[T]) {

	if mat.rowLen != other.numRows || mat.numRows != other.rowLen {
		panic("bad matrix multiplication")
	}

	res = Matrix[T]{
		data:    make([]T, other.rowLen*mat.numRows),
		numRows: other.rowLen,
		rowLen:  mat.numRows,
	}

	for y := range mat.numRows {
		first := mat.Row(y)
		for x := range other.rowLen {
			second := other.Col(x)
			*res.Get(x, y) = first.Dot(second)
			fmt.Printf("Setting (%d, %d) to %f\n", x, y, first.Dot(second))
			fmt.Printf("First: %f\n", first)
			fmt.Printf("Second: %f\n", second)

		}
	}

	return res
}

func (mat Matrix[T]) VecMul(v Vector[T]) (res Vector[T]) {
	if len(v.data) != mat.rowLen {
		panic("bad matrix vector multiplication")
	}
	res = Vector[T]{make([]T, len(v.data))}
	for i := range mat.numRows {
		*res.At(i) = mat.Row(i).Dot(v)
	}
	return res
}

// Identity makes a square identity matrix
func Identity[T ArithmeticDefined](numRows int) (res Matrix[T]) {
	res = Matrix[T]{
		data:    make([]T, numRows*numRows),
		numRows: numRows,
		rowLen:  numRows,
	}

	for i := range numRows {
		*res.Get(i, i) = T(1)
	}

	return res
}

func Scale[T ArithmeticDefined](factors ...T) (res Matrix[T]) {
	numRows := len(factors)
	res = Matrix[T]{
		data:    make([]T, numRows*numRows),
		numRows: numRows,
		rowLen:  numRows,
	}

	for i := range numRows {
		*res.Get(i, i) = factors[i]
	}

	return res
}

func Translate[T ArithmeticDefined](factors ...T) (res Matrix[T]) {
	numRows := len(factors)
	res = Matrix[T]{
		data:    make([]T, numRows*numRows),
		numRows: numRows,
		rowLen:  numRows,
	}

	for i := range numRows {
		*res.Get(i, i) = 1.0
		*res.Get(numRows-1, i) = factors[i]
	}

	return res
}

func (mat Matrix[T]) Equal(other Matrix[T]) bool {
	if mat.rowLen != other.rowLen || mat.numRows != other.numRows {
		return false
	}

	for i := range len(mat.data) {
		if mat.data[i] != other.data[i] {
			return false
		}
	}

	return true
}

func (mat Matrix[T]) GoString() string {
	builder := strings.Builder{}

	for i := range mat.numRows {
		builder.WriteString(mat.Row(i).GoString())
		builder.WriteString("\n")
	}

	return builder.String()
}

func OrthographicProjection[T ArithmeticDefined](left, right, bottom, top, near, far T) Matrix[T] {
	translate := Translate[T](
		-(right+left)/2,
		-(top+bottom)/2,
		-(far+near)/2,
		1)

	scale := Scale[T](
		2/(right-left),
		2/(top-bottom),
		2/(far-near),
		1)
	return scale.Mul(translate)
}

// OrthographicProjection2 Potentially faster way of calculating orthographic projection
func orthographicProjection2[T ArithmeticDefined](left, right, bottom, top, near, far T) Matrix[T] {
	return MakeMatrix[T](4, 4,
		2/(right-left), 0, 0, -(right+left)/(right-left),
		0, 2/(top-bottom), 0, -(top+bottom)/(top-bottom),
		0, 0, 2/(far-near), -(far+near)/(far-near),
		0, 0, 0, 1)
}
