package math

import (
	"testing"
)

func TestMatrix3_Mul(t *testing.T) {
	one := MakeMatrix(
		3,
		3,
		1, 2, 3,
		4, 5, 6,
		7, 8, 9,
	)

	two := MakeMatrix(
		3,
		3,
		2, 2, 2, 2, 2, 2, 2, 2, 2,
	)

	res := one.Mul(two)
	if !res.Equal(MakeMatrix(3, 3,
		12, 12, 12,
		30, 30, 30,
		48, 48, 48)) {
		t.Errorf("Incorrect matrix multiplication:\n%#v \n* \n%#v \n!= \n%#v", one, two, res)
	}
}
