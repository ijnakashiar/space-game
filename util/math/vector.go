package math

import (
	"fmt"
	"math"
	"math/rand"
	"strings"
)

type Vector[T ArithmeticDefined] struct {
	data []T
}

func (v Vector[T]) At(i int) *T {
	return &v.data[i]
}

func MakeVector[T ArithmeticDefined](elements ...T) Vector[T] {
	return Vector[T]{elements}
}

func GetZeroVector[T ArithmeticDefined](elementCount int) Vector[T] {
	return Vector[T]{make([]T, elementCount)}
}

func GetRandomVector[T ArithmeticDefined](elementCount int) (vec Vector[T]) {
	vec = Vector[T]{make([]T, elementCount)}
	for i := range elementCount {
		*vec.At(i) = T(rand.Float32())
	}
	return vec
}

func (v Vector[T]) Length() float64 {
	return math.Sqrt(float64(v.LengthSqr()))
}

func (v Vector[T]) LengthSqr() (sum T) {
	for i := range len(v.data) {
		sum += *v.At(i) * *v.At(i)
	}
	return sum
}

type BinOpFunc[T ArithmeticDefined] func(op1, op2 T) T

type ReduceFunc[T ArithmeticDefined] func(acc, cur T, itr int) T

func (v Vector[T]) Op(other Vector[T], fn BinOpFunc[T]) (vec Vector[T]) {
	if len(v.data) != len(other.data) {
		panic("Vectors are not the same length")
	}
	vec = Vector[T]{make([]T, len(v.data))}

	for i := range len(v.data) {
		vec.data[i] = fn(v.data[i], other.data[i])
	}

	return vec
}

func (v Vector[T]) Reduce(reduceFunc ReduceFunc[T]) (acc T) {
	for i := range len(v.data) {
		acc = reduceFunc(acc, v.data[i], i)
	}
	return acc
}

func (v Vector[T]) Add(other Vector[T]) Vector[T] {
	return v.Op(other, func(op1, op2 T) T {
		return op1 + op2
	})
}

func (v Vector[T]) Sub(other Vector[T]) Vector[T] {
	return v.Op(other, func(op1, op2 T) T {
		return op1 - op2
	})
}

func (v Vector[T]) Mul(other Vector[T]) Vector[T] {
	return v.Op(other, func(op1, op2 T) T {
		return op1 * op2
	})
}

func (v Vector[T]) Div(other Vector[T]) Vector[T] {
	return v.Op(other, func(op1, op2 T) T {
		return op1 / op2
	})
}

func (v Vector[T]) Dot(other Vector[T]) T {
	return v.Reduce(func(acc T, cur T, itr int) T {
		return acc + cur*other.data[itr]
	})
}

func (v Vector[T]) Equal(other Vector[T]) bool {
	if len(v.data) != len(other.data) {
		panic("Vectors are not the same length")
	}
	for i := range len(v.data) {
		if v.data[i] != other.data[i] {
			return false
		}
	}
	return true
}

func (v Vector[T]) GoString() string {
	builder := strings.Builder{}
	for i := range len(v.data) {
		builder.WriteString(fmt.Sprintf("%#v", v.data[i]))
		builder.WriteString(" ")
	}
	return builder.String()
}
