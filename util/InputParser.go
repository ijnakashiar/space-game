package util

import (
	"encoding/json"
	"fmt"
	"io/fs"
)

func ParseNamesFile(file fs.File) (Names, error) {
	stat, err := file.Stat()
	if err != nil {
		return Names{}, err
	}

	byteArray := make([]byte, stat.Size())
	_, err = file.Read(byteArray)

	fmt.Println(string(byteArray))

	if err != nil {
		return Names{}, err
	}

	res := Names{}

	err = json.Unmarshal(byteArray, &res)
	if err != nil {
		return Names{}, err
	}

	return res, nil
}
