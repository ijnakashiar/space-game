package image

// #define STB_IMAGE_IMPLEMENTATION
// #include "stb_image.h"
import "C"
import (
	"fmt"
	"unsafe"
)

type Image struct {
	Data       []byte
	X          int
	Y          int
	Components int
}

func (i Image) GetPixel(x int, y int) ([]byte, error) {
	if x >= i.X && y >= i.Y {
		return []byte{}, fmt.Errorf("bad access to pixel at: %d %d", x, y)
	}

	loc := i.X*i.Components*y + x*i.Components
	return i.Data[loc : loc+3], nil
}

func (i Image) SumPixelValues(channel int) (sum uint64) {
	for j := channel; j < i.X*i.Y*i.Components; j += i.Components {
		sum += uint64(i.Data[j])
	}
	return sum
}

func (i Image) SumRow(channel int, y int) (sum uint64, err error) {
	for x := 0; x < i.X; x++ {
		pixel, err := i.GetPixel(x, y)
		if err != nil {
			return 0, err
		}
		sum += uint64(pixel[channel])
	}
	return sum, err
}

func (i Image) SumColumn(channel int, x int) (sum uint64, err error) {
	for y := 0; y < i.Y; y++ {
		pixel, err := i.GetPixel(x, y)
		if err != nil {
			return 0, err
		}
		sum += uint64(pixel[channel])
	}
	return sum, err
}

func LoadImage(filepath string, components int) Image {
	var x C.int
	var y C.int
	var n C.int // components
	cPtr := C.stbi_load(C.CString(filepath), &x, &y, &n, C.int(components))
	imageData := C.GoBytes(unsafe.Pointer(cPtr), x*y*n)
	return Image{imageData[:], int(x), int(y), int(n)}
}
