package random

import (
	"fmt"
	"math/rand"
	"spacegame/util/image"
)

// TODO: Write a function that generates a n-dimensional probability function based on an input n-dim vector.
// TODO: Sample the probability image using inverse sampling. Use it to generate random vectors based on the pmf

// InverseSampleDiscreteFunction Given a 2D pmf, sample a value using a random process that follows the given pmf.
func InverseSampleDiscreteFunction(pmf [][]float32) int {
	randomNum := rand.Float32()
	fmt.Println(randomNum)
	return 0
}

// GetPMFFromImage given a 2D image, generate a 2D pmf based on the luminosity of the image.
func GetPMFFromImage(image image.Image) (pmf [][]float32) {
	// It should be two-dimensional.
	return pmf
}
