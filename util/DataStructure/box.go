package DataStructure

import "spacegame/util/math"

type AABBox[T math.ArithmeticDefined] struct {
	Begin math.Vector[T]
	End   math.Vector[T]
}
