package ship

import (
	"spacegame/util/math"
)

type Ship struct {
	Hardpoints []Hardpoint
	Health     int

	Radar struct {
		Strength int
		Range    int
	}

	Receiver struct {
		Strength int
	}

	Engine struct {
		Speed           int
		FuelConsumption int
	}

	Upgrades []bool // temporary
}

func (receiver Ship) GetTravelCost(distance math.Vector3[float32]) float64 {
	return distance.Length() * float64(receiver.Engine.FuelConsumption)
}

func (receiver Ship) GetTravelTime(distance math.Vector3[float32]) float64 {
	return distance.Length() / float64(receiver.Engine.Speed)
}

type HardpointType int

const (
	Weapon HardpointType = iota
)

type Hardpoint struct {
	Type   HardpointType
	Health int
}
