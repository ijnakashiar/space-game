package game

import "spacegame.go/game/world"

type State struct {
	CurrentSystem *world.StarSystem
	CurrentPlanet *world.Planet
	Time          int
}

func GameLoop() {

	// Render the UI

	// Handle input events

	// Update game state if input is updated
}
