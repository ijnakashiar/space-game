package game

// Player skills and health. Skills scale from 0 to 100.
type Player struct {
	Skills struct {
		// affects piloting and evasion
		Maneuvering int
		// affects decision probabilities
		Intelligence int
		// affects combat
		Combat int
		// affects radar detection and avoidance
		Perception int
	}

	Afflictions struct {
		Mind struct {
		}
	}
}

func (p Player) GetDetectionCoefficient() float32 {
	return float32(p.Skills.Perception)
}
