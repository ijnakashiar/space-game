package world

import (
	"errors"
	"math/rand"
	"spacegame/util/DataStructure"
	"spacegame/util/math"
)

type Galaxy struct {
	starSystems []StarSystem
}

type GalaxyGenerationConfig struct {
	StarSystemCount int
	MapSize         math.Vector[float32]
}

// FindClosestSystems Replace this with a better implementation later
func (g Galaxy) FindClosestSystems(s StarSystem) {

}

func GenerateGalaxy(config GalaxyGenerationConfig) (Galaxy, error) {
	var galaxy Galaxy

	for range config.StarSystemCount {
		starSystem, err := generateStarSystem(
			StarSystemConfig{
				Region: DataStructure.AABBox[float32]{
					Begin: math.GetZeroVector[float32](3),
					End:   config.MapSize,
				},
				NumStars:   rand.Intn(3),
				NumPlanets: rand.Intn(5),
			},
		)

		if err != nil {
			return Galaxy{}, errors.New("couldn't make star system")
		} else {
			galaxy.starSystems = append(galaxy.starSystems, starSystem)
		}
	}
	return galaxy, nil
}
