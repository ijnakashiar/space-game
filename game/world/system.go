package world

import (
	datastructure "spacegame/util/DataStructure"
	util "spacegame/util/math"
)

// Star systems

type StarSystem struct {
	stars     []Star
	planets   []Planet
	location  util.Vector[float32] // Location in the galaxy
	anomalies int
}

type StarSystemConfig struct {
	Region     datastructure.AABBox[float32]
	NumStars   int
	NumPlanets int
}

func generateStarSystem(config StarSystemConfig) (StarSystem, error) {
	var starSystem StarSystem
	var stars []Star
	var planets []Planet

	randPos := util.GetRandomVector[float32](3).Mul(config.Region.End.Sub(config.Region.Begin)).Add(config.Region.Begin)

	for range config.NumStars {
		stars = append(stars, GenerateRandomStar(&starSystem))
	}

	for range config.NumPlanets {
		planets = append(planets, GenerateRandomPlanet(&starSystem))
	}

	return StarSystem{
		stars:    stars,
		planets:  planets,
		location: randPos,
	}, nil
}
