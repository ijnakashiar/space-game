package world

import "math/rand"

var StarTypes = []StarType{
	StarType{
		"O", "Some of the hottest stars in the galaxy. They emit a bright blue hue. Very rare. Approach with extreme caution.",
	},
	StarType{
		"B", "Very hot stars that emit a blue hue. Very rare. Approach with caution.",
	},
	StarType{
		"A", "Very hot stars that emit a white hue. Very rare.",
	},
	StarType{
		"F", "Stars that emit a white hue. Relatively rare.",
	},
	StarType{
		"G", "Stars that emit a yellowish-white hue. Relatively rare.",
	},
	StarType{
		"K", "Stars that emit an orange hue. Relatively common.",
	},
	StarType{
		"M", "Stars that emit a deep orange hue. Very common.",
	},
}

type StarType struct {
	Classification string
	Description    string
}

type Star struct {
	System   *StarSystem
	Size     float32
	StarType StarType
}

func GenerateRandomStar(system *StarSystem) Star {
	return Star{
		System:   system,
		Size:     rand.Float32() * 1000, // The range should be tied with the star type
		StarType: StarTypes[rand.Intn(len(StarTypes))],
	}
}
