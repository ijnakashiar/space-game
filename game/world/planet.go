package world

import "math/rand"

type Planet struct {
	system      *StarSystem
	name        string
	size        float32
	gravity     float32
	orbitRadius float32
}

func GenerateRandomPlanet(starSystem *StarSystem) Planet {
	var name = make([]rune, 9)
	for i := range 6 {
		name[i] = rune(rand.Intn(26) + 65)
	}

	name[6] = 45

	for i := range 2 {
		name[i+7] = rune(rand.Intn(10) + 48)
	}

	return Planet{
		system:      starSystem,
		name:        string(name),
		size:        rand.Float32() * 1000,
		gravity:     rand.Float32() * 1000,
		orbitRadius: rand.Float32() * 1000,
	}
}
